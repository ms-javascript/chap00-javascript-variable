package com.mseop.javaScript.variable;

public class Application {
/*
	 자바스크립트
	
	1. 태그에 직접 자바스크립트 코드를 작성후 실행하게 되는 경우
	<button onclick="alert('버튼');">복습버튼</button>
	
	2. script태그 안에 함수 단위로 소스 작성후, 태그에서 on이벤트명="함수명();"으로 사용하는 경우
	<button onclick="testFunction();">함수실행확인버튼</button>
	
	<script>
		Function testFunction() {
	             window.alert("testFunction() 함수 실행됨.")
	           }
	</script>
	
	3. 별도의 작성된 자바스크립트 함수 사용 테스트
	<button onclick="testfn">별로버튼</button>
	
	3-1 js페이지
	function testfn() {
	 window.alert('testfn() 함수 실행됨..')
	}
	HTML 연동 맨위
	<script type="text/javacript" src="../js/sample.js"></script>
	
	
	* head 안에 script 에서 function start() {} 를 주고,
	  body onload="start();"> 를 주면 로딩이 다되면 위에 창이 뜨게된다.
	
	
	<h1>자바스크립트의 데이터 입/출력하기 </h1>
	<script>
		document.write("<h1>document.write로 출력함..</h1>")
	</script>
	화면상에 바로 띄어줄수 있음.
	
	* alert : 브라우저와 별도의 상관없이 창을 띄어준다. & 확인용
	
	
	* innerHTML로 요소의 내용을 변경하지
	- 자바스크립트에서 태그 앨리먼트의 값을 읽거나 & 값을 변경할때 사용한다.
	
	document.getElementById("area1");
	document( html전체 )
	getElementById ( Id로 찾겠다 )
	
	<script>
		let arear1 = document.getElementById("area1");
		alert(area1.innerHTML);
	</script>
	- 값 가져오기 arear1 에 area1에있는 키/밸류값을다 넣어주었고, 그것을 꺼내올땐
		      area1.innerHTML , area1.id 이런식으로 꺼내오게된다.
	
	그렇다면 변경? 넣어볼라면?
	arear1.innerHTML = "속성 값 변경함"; 해주면 완료
	
	
	* window.confirm() 어떤 질문에 대해 예/아니오 결과를 얻을때 사용한다
	  확인버튼과 취소버튼이 나타나며, 
	
	<script>
		function checkGender() {
		    let result = window.confirm("할말  쓰면된다.")
	                console.log(result);	
	            }
	</script>
	
	
	document.getElmenetById("area2"); Id로 area2인 문장을 찾아준다.
	let result = document.getElmenetById("area2"); 그문장을 result에 담아준다.
	reuslt.innerHTML += "당신은" + gender + "입니다.<br>" result에 innerHTML로 +=로 담아주준다.
	
	
	*  window.prompt() 입력값을 만들어준다.
	
	---------------------------------------------------------------------------------
	
	- 함수 -
	
	
	
	배열 합치기
	
	let area5 = document.getElementById("area5");
	
	let arr = ['소녀시대, '핑클', 'SES', '미쓰에이'];
	
	let arrJoin = arr.join();
	
	console.log(arrJoin);
	결과값 : 소녀시대,핑클,SES,미쓰에이 String으로 된다. 
	           arr의 자료형은 Object가 됨.
	
	indexOf() : 배열에서 요소가 위치하는 인덱스를 리턴
	concat(배열명) : 두개 혹은 세개의 배열을 결합할 때 사용을한다
	join(): 배열을 결합하여 문자열로 반환한다.
	reverse() : 배열의 순서를 뒤집는다. 
	sort() : 배열의 내림차순 혹은 오름차순으로 정렬한다.
	
	push() : 배열의 맨 뒤에 요소 추가
	pop() : 배열의 맨 뒤에 요소를 제거 하고 리턴
	shift() : 배열의 맨 앞에 요소 제거
	unshift() : 배열의 맨 앞에 요소 추가  
	slice() : 배열의 요소를 선택해서 잘라내기 /  slice(시작인덱스, 종료인덱스)
	splice() : 배열의 index위치의 요소 제거 및 추가도 가능하다. / splice([index], 제거수 ,추가값)
	
	 // arguments를 사용하면 매개변수를 사용하지 않아도 매개변수값을 확인할수 있다.
*/
}
